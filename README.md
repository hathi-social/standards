*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

#### Please see the wiki at:
https://gitlab.com/hathi-social/standards/wikis/home
